package de.uniwue.dmir.swd.core.request.parameter;

import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;

public class BoundingBox extends RequestParameter {

	public final static String PARAM_BBOX = "bbox";

	private de.uniwue.dmir.swd.core.features.primitives.BoundingBox bbox;
	
	public BoundingBox(){
		super(PARAM_BBOX);
	}
	
	public BoundingBox(BoundingBox other){
		this();
		this.bbox = other.bbox;
	}
	
	public BoundingBox(de.uniwue.dmir.swd.core.features.primitives.BoundingBox bbox){
		this();
		this.bbox = bbox;
	}
	
	public void setBoundingBox(de.uniwue.dmir.swd.core.features.primitives.BoundingBox bbox){
		this.bbox = bbox;
	}
	
	public de.uniwue.dmir.swd.core.features.primitives.BoundingBox getBoundingBox(){
		return bbox;
	}
	
	public boolean getIsNull()
	{
		return bbox == null;
	}
	
	
	
	@Override
	public Object getDefaultValue() {
		if( getIsNull() ){
			de.uniwue.dmir.swd.core.features.primitives.BoundingBox bb;
			de.uniwue.dmir.swd.core.features.primitives.Coordinate bottomLeft, topRight;
			
			bottomLeft = new de.uniwue.dmir.swd.core.features.primitives.Coordinate(-90, -180);
			topRight = new de.uniwue.dmir.swd.core.features.primitives.Coordinate(90, 180); 
			
			bb = new de.uniwue.dmir.swd.core.features.primitives.BoundingBox(bottomLeft, topRight);
			return bb;
		} else {
			return bbox;
		}
	}

	@Override
	public void setDefaultValue(Object defaultValue) {
		
		if( !(defaultValue instanceof de.uniwue.dmir.swd.core.features.primitives.BoundingBox)){
			throw new IllegalArgumentException("defaultValue is no instance of type de.uniwue.dmir.swd.core.features.primitives.BoundingBox");
		}
		
		this.bbox = (de.uniwue.dmir.swd.core.features.primitives.BoundingBox) defaultValue;
	}
	
	@Override
	public RequestParameter clone() {
		return new BoundingBox(this);
	}
	
	public RequestParameter parseInstance(String parameterValue) throws ParameterParseException{
		String[] bboxValues = parameterValue.split(",");
		
		de.uniwue.dmir.swd.core.features.primitives.Coordinate bottomLeft, topRight;
		
		if( bboxValues.length == 4){
			double latBottom, lonLeft, latTop, lonRight;
			latBottom = Double.parseDouble(bboxValues[0]);
			lonLeft = Double.parseDouble(bboxValues[1]);
			latTop = Double.parseDouble(bboxValues[2]);
			lonRight = Double.parseDouble(bboxValues[3]);
			
			bottomLeft = new de.uniwue.dmir.swd.core.features.primitives.Coordinate(latBottom, lonLeft);
			topRight = new de.uniwue.dmir.swd.core.features.primitives.Coordinate(latTop, lonRight);
		} else {
			throw new ParameterParseException();
		}
		
		de.uniwue.dmir.swd.core.features.primitives.BoundingBox bb
			= new de.uniwue.dmir.swd.core.features.primitives.BoundingBox(bottomLeft, topRight);
		
		return new BoundingBox(bb);
	}
}
