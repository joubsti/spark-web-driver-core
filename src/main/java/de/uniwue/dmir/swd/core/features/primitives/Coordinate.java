package de.uniwue.dmir.swd.core.features.primitives;

import java.io.Serializable;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class Coordinate implements KryoSerializable, Serializable{

	private static final long serialVersionUID = -7221126969054641841L;
	private double latitude;
	private double longitude;
	
	public Coordinate(double latitude, double longitude){
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public double getLatitude(){
		return latitude;
	}
	
	public double getLongitude(){
		return longitude;
	}
	
	public void setLatitude(double latitude){
		this.latitude = latitude;
	}
	
	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public void write(Kryo kryo, Output output) {
		output.writeDouble(latitude);
		output.writeDouble(longitude);
	}

	public void read(Kryo kryo, Input input) {
		latitude = input.readDouble();
		longitude = input.readDouble();
	}
}
