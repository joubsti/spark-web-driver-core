package de.uniwue.dmir.swd.core.request;

import de.uniwue.dmir.swd.core.request.parameter.RequestParameter;

public class RequestParameterEntry {
	private RequestParameter parameter;
	private boolean isDefault;
	
	RequestParameterEntry(RequestParameter parameter, boolean isDefault){
		this.parameter = parameter;
		this.isDefault = isDefault;
	}
	
	public RequestParameter getParameter(){
		return parameter;
	}
	
	public boolean getIsDefault(){
		return isDefault;
	}
}
