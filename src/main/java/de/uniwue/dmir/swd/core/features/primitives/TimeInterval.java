package de.uniwue.dmir.swd.core.features.primitives;

import java.util.Date;

public class TimeInterval {
	private Date from;
	private Date until;
	
	public TimeInterval(Date from, Date until){
		this.from = from;
		this.until = until;
	}
	
	public Date getFrom(){
		return from;
	}

	public void setFrom(Date from){
		this.from = from;
	}
	
	public Date getUntil(){
		return until;
	}

	public void setUntil(Date until){
		this.until = until;
	}
}
