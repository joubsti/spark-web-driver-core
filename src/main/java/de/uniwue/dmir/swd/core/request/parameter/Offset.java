package de.uniwue.dmir.swd.core.request.parameter;

import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;

public class Offset extends RequestParameter {
	public final static String PARAM_OFFSET = "offset";
	
	private Integer offset = 0;
	
	public Offset() {
		super(PARAM_OFFSET);
	}
	
	public Offset(Offset other){
		this();
		this.offset = other.offset;
	}
	
	public Offset(int offset){
		super(PARAM_OFFSET);
		this.offset = offset;
	}

	public void setOffset(int offset){
		this.offset = offset;
	}
	
	public int getOffset(){
		return offset;
	}
	
	public boolean getIsNull()
	{
		return offset == null;
	}
	
	@Override
	public Object getDefaultValue() {
		if( getIsNull() ){
			return 0;
		} else {
			return offset;
		}
	}

	@Override
	public void setDefaultValue(Object defaultValue) {
		
		if( !(defaultValue instanceof Integer)){
			throw new IllegalArgumentException("defaultValue is no instance of type Integer");
		}
		
		this.offset = (Integer) defaultValue;
	}
	
	@Override
	public RequestParameter clone() {
		return new Offset(this);
	}
	
	public RequestParameter parseInstance(String parameterValue) throws ParameterParseException{
		
		int o;
		
		if( parameterValue != null ){
			o = Integer.parseInt(parameterValue);
		} else {
			throw new ParameterParseException();
		}
		
		
		return new Offset(o);
	}
}
