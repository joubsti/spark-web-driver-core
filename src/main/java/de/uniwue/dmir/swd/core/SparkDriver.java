package de.uniwue.dmir.swd.core;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.util.thread.QueuedThreadPool;


public class SparkDriver 
{
	Server server;
	ContextHandlerCollection contexts;
	String appName;
	
	public SparkDriver(int port, String appName){
		this.server = new Server(port);
		this.contexts = new ContextHandlerCollection();
		this.appName = appName;
	}
	
	public Server getServer(){
		return server;
	}
	
	public ContextHandler addHandler(String contextPath, Handler handler){
		ContextHandler flickrDataContext = new ContextHandler(contextPath);
        flickrDataContext.setHandler(handler);
        contexts.addHandler(flickrDataContext);
        return flickrDataContext;
	}
	
	public void createThreadPool(int minThreads, int maxThreads){
		QueuedThreadPool threadPool = new QueuedThreadPool();
		threadPool.setMinThreads(5);
		threadPool.setMaxThreads(20);
		server.getServer().setThreadPool(threadPool);
	}
	
	public void start(){
		server.setHandler(contexts);
		
		// Start things up!
        try {
			server.start();
		} catch (Exception e2) {
			e2.printStackTrace();
		}

        try {
			server.join();
		} catch (InterruptedException e2) {
			e2.printStackTrace();
		}
        
        System.out.println(appName + "-App Started");
	    
	    while(true){
	    	try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	    }
	}
}
