package de.uniwue.dmir.swd.core.features.util.intervalizer;

public interface IIntervalizer<T> {

	int toIntervalId(T input);
	T fromIntervalId (int interval);
	int numberOfBins();
}

