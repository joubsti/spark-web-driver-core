package de.uniwue.dmir.swd.core.features.parser;

public interface IParser<T> {
	public String parseParsable(T parsable);
}
