package de.uniwue.dmir.swd.core.request.parameter;

import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;

public class Coordinate extends RequestParameter {

	public final static String PARAM_COORDINATE = "coordinate";
	
	private de.uniwue.dmir.swd.core.features.primitives.Coordinate coordinate;
	
	public Coordinate(){
		super(PARAM_COORDINATE);
	}
	
	public Coordinate(Coordinate other){
		this();
		this.coordinate = other.coordinate;
	}
	
	public Coordinate(de.uniwue.dmir.swd.core.features.primitives.Coordinate coordinate){
		this();
		this.coordinate = coordinate;
	}
	
	public void setCoordinate(de.uniwue.dmir.swd.core.features.primitives.Coordinate coordinate){
		this.coordinate = coordinate;
	}
	
	public de.uniwue.dmir.swd.core.features.primitives.Coordinate getCoordinate(){
		return coordinate;
	}
	
	public boolean getIsNull()
	{
		return coordinate == null;
	}
	
	@Override
	public Object getDefaultValue() {
		if( getIsNull() ){
			de.uniwue.dmir.swd.core.features.primitives.Coordinate co;
			co = new de.uniwue.dmir.swd.core.features.primitives.Coordinate(0, 0);
			return co;
		} else {
			return coordinate;
		}
	}

	@Override
	public void setDefaultValue(Object defaultValue) {	
		if( !(defaultValue instanceof de.uniwue.dmir.swd.core.features.primitives.Coordinate)){
			throw new IllegalArgumentException("defaultValue is no instance of type de.uniwue.dmir.swd.core.features.primitives.Coordinate");
		}
		
		this.coordinate = (de.uniwue.dmir.swd.core.features.primitives.Coordinate) defaultValue;
	}
	
	@Override
	public RequestParameter clone() {
		return new Coordinate(this);
	}
	
	public RequestParameter parseInstance(String parameterValue) throws ParameterParseException{
		String[] coordinateValues = parameterValue.split(",");
		
		de.uniwue.dmir.swd.core.features.primitives.Coordinate c;
		
		if( coordinateValues.length == 2){
			double latitude, longitude;
			latitude = Double.parseDouble(coordinateValues[0]);
			longitude = Double.parseDouble(coordinateValues[1]);
			
			c = new de.uniwue.dmir.swd.core.features.primitives.Coordinate(latitude, longitude);
		} else {
			throw new ParameterParseException();
		}
		
		return new Coordinate(c);
	}
}
