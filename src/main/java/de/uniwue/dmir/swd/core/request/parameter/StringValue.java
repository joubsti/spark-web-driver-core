package de.uniwue.dmir.swd.core.request.parameter;

import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;

public class StringValue extends RequestParameter {
	
	private String value;
	
	public StringValue(String name) {
		super(name);
	}
	
	public StringValue(String name, String value) {
		super(name);
		this.value = value;
	}
	
	public StringValue(StringValue other){
		this(other.getName());
		this.setIsDefault(other.getIsDefault());
		this.value = other.value;
	}

	public void setValue(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}
	
	public boolean getIsNull()
	{
		return value.isEmpty();
	}
	
	@Override
	public Object getDefaultValue() {
		if( getIsNull() ){
			return "";
		} else {
			return value;
		}
	}

	@Override
	public void setDefaultValue(Object defaultValue) {
		
		if( !(defaultValue instanceof String)){
			throw new IllegalArgumentException("defaultValue is no instance of type String");
		}
		
		this.value = (String) defaultValue;
	}
	
	@Override
	public RequestParameter clone() {
		return new StringValue(this);
	}
	
	public RequestParameter parseInstance(String parameterValue) throws ParameterParseException{
		return new StringValue(this.getName(), parameterValue);
	}

}
