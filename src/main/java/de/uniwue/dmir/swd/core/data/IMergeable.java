package de.uniwue.dmir.swd.core.data;

public interface IMergeable<T>{
	
	public T merge(T other);

}
