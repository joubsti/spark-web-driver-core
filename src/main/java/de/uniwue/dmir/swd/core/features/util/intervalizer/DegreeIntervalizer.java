package de.uniwue.dmir.swd.core.features.util.intervalizer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.uniwue.dmir.swd.core.features.primitives.BoundingBox;
import de.uniwue.dmir.swd.core.features.primitives.Coordinate;

public class DegreeIntervalizer implements ICoordinateIntervalizer, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9109532095072044671L;

	protected BoundingBox boundingBox;

	private double diffLat;
	private double diffLon;
	
	private int width;
	private int height;
	
	private double overlap;
	
	public DegreeIntervalizer(
			BoundingBox boundingBox,
			double diffLon, 
			double diffLat) {
		
		this.diffLat = diffLat;
		this.diffLon = diffLon;
		this.overlap = 0;
		
		this.setBoundingBox(boundingBox);
	}
	
	public DegreeIntervalizer(
			BoundingBox boundingBox,
			double offset)
	{	
		this.diffLon = 64.0 / Math.pow(2.0, offset);
		this.diffLat = this.diffLon / 2;
		this.overlap = 0;
		
		this.setBoundingBox(boundingBox);
	}
	
	public void setBoundingBox(BoundingBox boundingBox){
		this.boundingBox = boundingBox;
		
		double width = this.boundingBox.getLonRight() - this.boundingBox.getLonLeft();
		double height = this.boundingBox.getLatTop() - this.boundingBox.getLatBottom();
		
		this.width = (int) Math.ceil(width / this.diffLon);
		this.height = (int) Math.ceil(height/ this.diffLat);
	}
	
	public double getDiffLon(){
		return this.diffLon;
	}
	
	public double getDiffLat(){
		return this.diffLat;
	}
	
	public int getWidth(){
		return this.width;
	}
	
	public int getHeight(){
		return this.height;
	}
	
	public void setOverlap(double overlap){
		this.overlap = overlap;
	}
	
	public double getOverlap(){
		return overlap;
	}
	
	public List<Integer> toIntervalIds(double latitude, double longitude){
		if (!this.boundingBox.contains(latitude, longitude)) {
			return new ArrayList<Integer>();
		}
		
		ArrayList<Integer> invervalIds = new ArrayList<Integer>();
		
		double latitudeTranslated = latitude - this.boundingBox.getLatBottom();
		double longitudeTranslated = longitude - this.boundingBox.getLonLeft();
		
		int latIndex = (int) Math.floor(latitudeTranslated / this.diffLat);
		int lonIndex = (int) Math.floor(longitudeTranslated / this.diffLon);

		invervalIds.add(latIndex * this.width + lonIndex);
		
		if( overlap > 0 ){
			for( int latIndex_o = latIndex - 1; latIndex_o <= latIndex + 1; latIndex_o++){
				for( int lonIndex_o = lonIndex - 1; lonIndex_o <= lonIndex + 1; lonIndex_o++){
					
					if( latIndex_o == latIndex && lonIndex_o == lonIndex)
						continue;
					
					int index_o = latIndex_o * this.width + lonIndex_o;
					
					Coordinate c = this.fromIntervalId(index_o);
					
					if( (c.getLatitude() - (this.diffLat / 2 + overlap)) <= latitude){
						invervalIds.add(index_o);
					} else if((c.getLatitude() + (this.diffLat / 2 + overlap)) >= latitude){
						invervalIds.add(index_o);
					} else if(( c.getLongitude() - (this.diffLon / 2 + overlap)) <= longitude){
						invervalIds.add(index_o);
					} else if(( c.getLongitude() + (this.diffLon / 2 + overlap)) >= longitude){
						invervalIds.add(index_o);
					}
						
				}
			}
		}
		
		
		return invervalIds;
		
	}
	
	public int toIntervalId(double latitude, double longitude) {
		
		if (!this.boundingBox.contains(latitude, longitude)) {
			return -1;
		}
		
		double latitudeTranslated = latitude - this.boundingBox.getLatBottom();
		double longitudeTranslated = longitude - this.boundingBox.getLonLeft();
		
		int latIndex = (int) Math.floor(latitudeTranslated / this.diffLat);
		int lonIndex = (int) Math.floor(longitudeTranslated / this.diffLon);
		
		int index = latIndex * this.width + lonIndex;
		
		return index;
	}
	
	public int toIntervalId(Coordinate input) {
		return this.toIntervalId(input.getLatitude(), input.getLongitude());
	}
	
	public Coordinate fromIntervalId(int index) {
		
		int latIndex = index / this.width;
		int lonIndex = index - (latIndex * this.width);
		
		return new Coordinate(
				this.boundingBox.getLatBottom() + this.diffLat * latIndex + this.diffLat / 2,
				this.boundingBox.getLonLeft() + this.diffLon * lonIndex + this.diffLon / 2);
	}
	
	public BoundingBox toBoundingBox(int index){
		Coordinate c = fromIntervalId(index);
		
		double bottom = Math.round((c.getLatitude()-this.diffLat/2) * 10000.0) / 10000.0;
		double left = Math.round((c.getLongitude()-this.diffLon/2) * 10000.0) / 10000.0;
		
		return new BoundingBox(bottom, left, bottom+this.diffLat, left+this.diffLon);
	}
	
	public int numberOfBins() {
		return this.width * this.height;
	}


}
