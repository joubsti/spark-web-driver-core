package de.uniwue.dmir.swd.core.request.parameter;

import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;

public class Overlap extends RequestParameter {
	public final static String PARAM_OVERLAP = "overlap";
	
	private Integer overlap = 0;
	
	public Overlap() {
		super(PARAM_OVERLAP);
	}
	
	public Overlap(Overlap other){
		this();
		this.overlap = other.overlap;
	}
	
	public Overlap(int overlap){
		this();
		this.overlap = overlap;
	}
	
	public void setOverlap(int overlap){
		this.overlap = overlap;
	}
	
	public int getOverlap(){
		return overlap;
	}
	
	public boolean getIsNull()
	{
		return overlap == null;
	}
	
	@Override
	public Object getDefaultValue() {
		if( getIsNull() ){
			return 0;
		} else {
			return overlap;
		}
	}

	@Override
	public void setDefaultValue(Object defaultValue) {
		
		if( !(defaultValue instanceof Integer)){
			throw new IllegalArgumentException("defaultValue is no instance of type Integer");
		}
		
		this.overlap = (Integer) defaultValue;
	}
	
	@Override
	public RequestParameter clone() {
		return new Overlap(this);
	}
	
	public RequestParameter parseInstance(String parameterValue) throws ParameterParseException{
		
		int o;
		
		if( parameterValue != null ){
			o = Integer.parseInt(parameterValue);
		} else {
			throw new ParameterParseException();
		}
		
		return new Overlap(o);
	}
}
