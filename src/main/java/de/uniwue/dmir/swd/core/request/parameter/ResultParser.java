package de.uniwue.dmir.swd.core.request.parameter;

import de.uniwue.dmir.swd.core.features.parser.JsonParser;
import de.uniwue.dmir.swd.core.features.parser.Parser;
import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;

public class ResultParser extends RequestParameter{

	public final static String PARAM_RESULTPARSER = "resultparser";
	private de.uniwue.dmir.swd.core.features.parser.Parser parser;
	
	public ResultParser() {
		super(PARAM_RESULTPARSER);
	}
	
	public ResultParser(ResultParser other) {
		this();
		this.parser = other.parser;
	}
	
	public ResultParser(de.uniwue.dmir.swd.core.features.parser.Parser parser) {
		this();
		this.parser = parser;
	}

	@Override
	public boolean getIsNull() {
		return parser == null;
	}

	@Override
	public Object getDefaultValue() {
		if( getIsNull() ){
			return new JsonParser();
		} else {
			return parser;
		}
	}
	
	@Override
	public RequestParameter clone() {
		return new ResultParser(this);
	}

	@Override
	public void setDefaultValue(Object defaultValue) {
		if( !(defaultValue instanceof Parser)){
			throw new IllegalArgumentException("defaultValue is no instance of type IParser");
		}
		
		this.parser = (Parser) defaultValue;
	}
	
	public Parser getParser(){
		return this.parser;
	}

	public RequestParameter parseInstance(String parameterValue) throws ParameterParseException {
		ResultParser p;
		
		if( parameterValue == "json"){
			p = new ResultParser();
		} else {
			throw new ParameterParseException();
		}
		
		return p;
	}

}
