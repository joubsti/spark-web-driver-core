package de.uniwue.dmir.swd.core.features.parser;

public class JsonParser extends Parser implements IParser<IJsonParsable> {

	public String parse(Object parsable){
		
		if( !(parsable instanceof IJsonParsable))
			throw new IllegalArgumentException("parsable is not an instance of IJsonParsable");
		
		return parseParsable((IJsonParsable) parsable);
	}
	
	public String parseParsable(IJsonParsable parsable){
		return parsable.parseToJson();
	}
	
}
