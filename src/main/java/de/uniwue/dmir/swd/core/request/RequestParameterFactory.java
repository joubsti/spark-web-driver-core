package de.uniwue.dmir.swd.core.request;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;
import de.uniwue.dmir.swd.core.request.parameter.RequestParameter;

public class RequestParameterFactory {

	public static RequestParameterList create(HttpServletRequest request, RequestParameterList bindableParameters) throws ParameterParseException, ParseException{
		RequestParameterList parameters = new RequestParameterList();
		
		for(RequestParameter p: bindableParameters){
			if( request.getParameterMap().containsKey(p.getName())){
				String parameter = request.getParameter(p.getName());
				
				parameters.add(p.parseInstance(parameter));
			}
		}
		
		for(RequestParameter p: bindableParameters){
			if( p.getIsDefault() && !parameters.contains(p.getName())){
				
				RequestParameter defaultParam = p.clone();
				defaultParam.setDefaultValue(p.getDefaultValue());
				parameters.add(defaultParam);
			}
		}
	
		return parameters;
	}
}
