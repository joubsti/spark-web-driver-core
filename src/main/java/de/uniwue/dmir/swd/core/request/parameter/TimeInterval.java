package de.uniwue.dmir.swd.core.request.parameter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;

public class TimeInterval extends RequestParameter {
	public final static String PARAM_TIMEINTERVAL = "timeinterval";
	public final static String DATE_FORMAT_REGEX_ISO8601 = "^(19|20)[0-9][0-9]-(0[0-9]|1[0-2])-(0[1-9]|([12][0-9]|3[01]))T([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$";
	public final static String DEFAULT_FIELD_NAME = "timestamp";		
	
	private de.uniwue.dmir.swd.core.features.primitives.TimeInterval timeInterval;
	private String fieldname = DEFAULT_FIELD_NAME;
	
	public TimeInterval() {
		super(PARAM_TIMEINTERVAL);
	}
	
	public TimeInterval(TimeInterval other){
		this();
		this.timeInterval = other.timeInterval;
		this.fieldname = other.fieldname;
	}
	
	public TimeInterval(de.uniwue.dmir.swd.core.features.primitives.TimeInterval timeInterval){
		this();
		this.timeInterval = timeInterval;
	}
	
	public TimeInterval(Date from, Date until){
		this(new de.uniwue.dmir.swd.core.features.primitives.TimeInterval(from, until));
	}
	
	public TimeInterval(Date from, Date until, String fieldname){
		this(from, until);
		this.fieldname = fieldname;
	}
	
	public de.uniwue.dmir.swd.core.features.primitives.TimeInterval getTimeInterval(){
		return timeInterval;
	}
	
	public String getFieldname(){
		return fieldname;
	}

	public void setFieldname(String fieldname){
		this.fieldname = fieldname;
	}
	
	public static Date parseTimeStringIso8601(String timeString) throws ParseException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		return dateFormat.parse(timeString);
	}
	
	public static Date parseTimeString(String timeString) throws ParseException, ParameterParseException {
		
		Date d;
		
		if( Pattern.matches( DATE_FORMAT_REGEX_ISO8601, timeString )){
			d = parseTimeString(timeString);
		} else if( Pattern.matches("\\d*", timeString)) {
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(Long.parseLong(timeString)*1000);
			d = c.getTime();
		} else {
			throw new ParameterParseException();
		}
		
		return d;
	}
	
	public RequestParameter parseInstance(String parameterValue) throws ParameterParseException{
		String[] timeValues = parameterValue.split(",");
		Date from, until;
		String fieldname = DEFAULT_FIELD_NAME;
		
		if( timeValues.length >= 2 ){
			try {
				from = parseTimeString(timeValues[0]);
				until = parseTimeString(timeValues[1]);
			} catch (ParseException e) {
				throw new ParameterParseException();
			}
			
		} else {
			throw new ParameterParseException();
		}
		
		if( timeValues.length == 3 ){
			fieldname = timeValues[2];
		}
		
		return new TimeInterval(from, until, fieldname);
	}

	@Override
	public boolean getIsNull() {
		return timeInterval == null;
	}

	@Override
	public Object getDefaultValue() {
		if( getIsNull() ){
			Date now = new Date();
			return new de.uniwue.dmir.swd.core.features.primitives.TimeInterval(now, now);
		} else {
			return timeInterval;
		}
	}
	
	@Override
	public RequestParameter clone() {
		return new TimeInterval(this);
	}

	@Override
	public void setDefaultValue(Object defaultValue) {
		if( !(defaultValue instanceof de.uniwue.dmir.swd.core.features.primitives.TimeInterval)){
			throw new IllegalArgumentException("defaultValue is no instance of type de.uniwue.dmir.swd.core.features.primitives.TimeInterval");
		}
		
		this.timeInterval = (de.uniwue.dmir.swd.core.features.primitives.TimeInterval) defaultValue;

	}
}
