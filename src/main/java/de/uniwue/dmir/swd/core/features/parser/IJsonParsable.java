package de.uniwue.dmir.swd.core.features.parser;

public interface IJsonParsable {
	public String parseToJson();
}
