package de.uniwue.dmir.swd.core.task;

import java.io.Serializable;

import de.uniwue.dmir.swd.core.request.SparkRequest;
import de.uniwue.dmir.swd.core.task.data.TaskData;

public abstract class SparkTask implements Serializable {
	private static final long serialVersionUID = 6099689379066284610L;

	private TaskData data;
	
	public SparkTask(TaskData data){
		this.data = data;
	}
	
	public TaskData getData(){
		return data;
	}
	
	public abstract Object process(SparkRequest request);
	
}
