package de.uniwue.dmir.swd.core.features.util.intervalizer;

import de.uniwue.dmir.swd.core.features.primitives.Coordinate;

public interface ICoordinateIntervalizer extends IIntervalizer<Coordinate> {
	
	public int toIntervalId(double latitude, double longitude);
	
}
