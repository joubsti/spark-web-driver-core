package de.uniwue.dmir.swd.core.request.parameter;

import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;

public abstract class RequestParameter implements Cloneable{

	private String name;
	private boolean isDefault;
	
	RequestParameter(String name){
		if( name == null ){
			throw new IllegalArgumentException();
		}
		
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public boolean getIsDefault(){
		return isDefault;
	}
	
	public void setIsDefault(boolean isDefault){
		this.isDefault = isDefault;
	}
	
	public abstract RequestParameter parseInstance(String parameterValue)  throws ParameterParseException;
	public abstract boolean getIsNull();
	public abstract Object getDefaultValue();
	public abstract void setDefaultValue(Object defaultValue);
	public abstract RequestParameter clone();
}
