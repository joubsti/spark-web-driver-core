package de.uniwue.dmir.swd.core.features.primitives;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonBuilderFactory;

import org.apache.spark.sql.DataFrame;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

public class BoundingBox implements KryoSerializable, Serializable{

	private static final long serialVersionUID = -7318321003199808624L;
	private Coordinate bottomLeft;
	private Coordinate topRight;
	
	public BoundingBox(){
		this(-90, -180, 90, 180);
	}
	
	public BoundingBox(double bottom, double left, double top, double right){
		this(new Coordinate(bottom, left), new Coordinate(top, right));
	}
	
	public BoundingBox(Coordinate bottomLeft, Coordinate topRight){
		
		if( bottomLeft == null )
			throw new IllegalArgumentException("Argument bottomLeft is null");
		
		if( topRight == null )
			throw new IllegalArgumentException("Argument topRight is null");
		
		this.bottomLeft = bottomLeft;
		this.topRight = topRight;
	}
	
	public boolean contains(double lat, double lon) {
	
		if (lat > getLatTop() || lat < getLatBottom()) {
			return false;
		} else if (lon > getLonRight()|| lon < getLonLeft()) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean contains(Coordinate co) {
		return contains(co.getLatitude(), co.getLongitude());
	}
	
	public BoundingBox extendedBy(Coordinate co){
		if( co.getLatitude() < this.getLatBottom() ){
			this.setLatBottom(co.getLatitude());
		}
		
		if( co.getLatitude() > this.getLatTop() ){
			this.setLatTop(co.getLatitude());
		}
		
		if( co.getLongitude() < this.getLonLeft() ){
			this.setLonLeft(co.getLongitude());
		}
		
		if( co.getLongitude() > this.getLonRight() ){
			this.setLonRight(co.getLongitude());
		}
		
		return this;
	}
	
	public BoundingBox extendedBy(BoundingBox bbox){
		if( bbox.getLatBottom() < this.getLatBottom() ){
			this.setLatBottom(bbox.getLatBottom());
		}
		
		if( bbox.getLatTop() > this.getLatTop() ){
			this.setLatTop(bbox.getLatTop());
		}
		
		if( bbox.getLonLeft() < this.getLonLeft() ){
			this.setLonLeft(bbox.getLonLeft());
		}
		
		if( bbox.getLonRight() > this.getLonRight() ){
			this.setLonRight(bbox.getLonRight());
		}
		
		return this;
	}
	
	public double getLatBottom(){
		return bottomLeft.getLatitude();
	}
	
	public double getLatTop(){
		return topRight.getLatitude();
	}
	
	public double getLonLeft(){
		return bottomLeft.getLongitude();
	}
	
	public double getLonRight(){
		return topRight.getLongitude();
	}
	
	public void setLatBottom(double latBottom){
		bottomLeft.setLatitude(latBottom);
	}
	
	public void setLatTop(double latTop){
		topRight.setLatitude(latTop);
	}
	
	public void setLonLeft(double lonLeft){
		bottomLeft.setLongitude(lonLeft);
	}
	
	public void setLonRight(double lonRight){
		topRight.setLongitude(lonRight);
	}
	
	public JsonArray toJsonArray(){
		Map<String, Object> config = new HashMap<String, Object>();
		JsonBuilderFactory factory = Json.createBuilderFactory(config);
		
		return factory.createArrayBuilder()
		.add(getLatBottom())
		.add(getLonLeft())
		.add(getLatTop())
		.add(getLonRight())
        .build();
	}
	
	public void normalizeFor(double diffLat, double diffLon){
		double lonLeft = Math.round(this.getLonLeft()*10000 - diffLon*10000) / 10000.0;
		double lonRight = Math.round(this.getLonRight()*10000 + diffLon*10000) / 10000.0;
		
		double latBottom = Math.round(this.getLatBottom()*10000 - diffLat*10000) / 10000.0;
		double latTop = Math.round(this.getLatTop()*10000 + diffLat*10000) / 10000.0;
		
		this.setLonLeft(lonLeft);
		this.setLonRight(lonRight);
		this.setLatBottom(latBottom);
		this.setLatTop(latTop);
	}
	
	public DataFrame applyToDataFrame(DataFrame df){
		
		if( getLonLeft() >= -180 ){
			df = df.where("longitude >= " + getLonLeft());
		}
		
		if( getLonRight() <= 180 ){
			df = df.where("longitude <= " + getLonRight());
		}
		
		if( getLatTop() <= 90 ){
			df = df.where("latitude <= " + getLatTop());
		}
		
		if( getLatBottom() >= -90 ){
			df = df.where("latitude >= " + getLatBottom());
		}
		
		return df;
	}

	public void write(Kryo kryo, Output output) {
	    kryo.writeObject(output, bottomLeft);
	    kryo.writeObject(output, topRight);
	}

	public void read(Kryo kryo, Input input) {
		bottomLeft = kryo.readObject(input, Coordinate.class);
		topRight = kryo.readObject(input, Coordinate.class);
	}
}
