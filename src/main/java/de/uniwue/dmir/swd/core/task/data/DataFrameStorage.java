package de.uniwue.dmir.swd.core.task.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.sql.DataFrame;

public class DataFrameStorage implements Serializable {
	private static final long serialVersionUID = -4184356967379087569L;
	private List<DataFrameEntry> dataFrames;
	
	public DataFrameStorage(){
		dataFrames = new ArrayList<DataFrameEntry>();
	}
	
	public boolean contains(String name){
		for( DataFrameEntry dfe: dataFrames){
			if( dfe.getName().equals(name) ){
				return true;
			}
		}
		return false;
	}
	
	public DataFrame getFrame(String name){
		for( DataFrameEntry dfe: dataFrames){
			if( dfe.getName().equals(name) ){
				return dfe.getDataFrame();
			}
		}
		return null;
	}
	
	public DataFrameEntry getEntry(String name){
		for( DataFrameEntry dfe: dataFrames){
			if( dfe.getName().equals(name) ){
				return dfe;
			}
		}
		return null;
	}
	
	public void add(String name, DataFrame dataframe){
		dataFrames.add(new DataFrameEntry(name, dataframe));
	}
}
