package de.uniwue.dmir.swd.core.features.parser;

public abstract class Parser {
	public abstract String parse(Object parsable);
}
