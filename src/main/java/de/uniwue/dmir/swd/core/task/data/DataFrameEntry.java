package de.uniwue.dmir.swd.core.task.data;

import java.io.Serializable;

import org.apache.spark.sql.DataFrame;

import de.uniwue.dmir.swd.core.features.primitives.BoundingBox;

public class DataFrameEntry implements Serializable {

	private static final long serialVersionUID = -646128064887901422L;
	private DataFrame dataframe;
	private String name;
	private BoundingBox bbox;
	
	public DataFrameEntry(String name, DataFrame dataframe){
		this.name = name;
		this.dataframe = dataframe;
	}
	
	public String getName(){
		return name;
	}
	
	public DataFrame getDataFrame(){
		return dataframe;
	}
	
	public BoundingBox getBoundingBox(){
		return bbox;
	}
	
	public void setBoundingBox(BoundingBox bbox){
		this.bbox = bbox;
	}
}
