package de.uniwue.dmir.swd.core.task;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;

import de.uniwue.dmir.swd.core.features.primitives.BoundingBox;
import de.uniwue.dmir.swd.core.features.primitives.Coordinate;
import de.uniwue.dmir.swd.core.request.SparkRequest;
import de.uniwue.dmir.swd.core.task.data.TaskData;

public class DataFrameToBoundingBoxTask extends SparkTask {

	private static final long serialVersionUID = 5669412849521112808L;

	public DataFrameToBoundingBoxTask(TaskData data) {
		super(data);
	}

	public BoundingBox process(SparkRequest request) {
		DataFrame df = (DataFrame) getData().get("dataframe");
		
		DataFrame lonLatdf = df.select("longitude", "latitude");
		
		JavaRDD<Row> jrd = lonLatdf.toJavaRDD();
		BoundingBox bbox = jrd.map(new Function<Row,Coordinate>(){

			private static final long serialVersionUID = -5547116772898074260L;

			public Coordinate call(Row row) throws Exception {

				double longitude = row.isNullAt(row.fieldIndex("longitude")) ? 0 : row.getDouble(row.fieldIndex("longitude"));
				double latitude  = row.isNullAt(row.fieldIndex("latitude")) ? 0 : row.getDouble(row.fieldIndex("latitude"));
				
				if( longitude == 0 || latitude == 0){
					return null;
				}
				
				return new Coordinate(latitude, longitude);
			}
			
		}).aggregate(new BoundingBox(0,0,0,0), new Function2<BoundingBox,Coordinate,BoundingBox>(){
			private static final long serialVersionUID = -8354430400273521324L;

			public BoundingBox call(BoundingBox v1, Coordinate v2) throws Exception {

				if(v2 != null){
					return v1.extendedBy(v2);
				}
				
				return v1;
			}
			
		}, new Function2<BoundingBox,BoundingBox,BoundingBox>(){
			private static final long serialVersionUID = 653253980419108688L;

			public BoundingBox call(BoundingBox v1, BoundingBox v2) throws Exception {
				return v1.extendedBy(v2);
			}
		});
		
		return bbox;
	}

}
