package de.uniwue.dmir.swd.core.request;

import java.util.ArrayList;

import de.uniwue.dmir.swd.core.request.parameter.RequestParameter;

public class RequestParameterList extends ArrayList<RequestParameter> {
	private static final long serialVersionUID = 8890484728627096526L;
	
	public boolean contains(String parameterName){
		
		for( RequestParameter rp: this){
			if( rp.getName() == parameterName ){
				return true;
			}
		}
		
		return false;
	}
	
	public void addDefault(RequestParameter parameter){
		parameter.setIsDefault(true);
		add(parameter);
	}
}
