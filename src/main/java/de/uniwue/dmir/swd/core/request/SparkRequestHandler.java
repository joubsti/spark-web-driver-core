package de.uniwue.dmir.swd.core.request;

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.handler.AbstractHandler;

import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;
import de.uniwue.dmir.swd.core.task.SparkTask;

public abstract class SparkRequestHandler extends AbstractHandler {

	//protected RequestParameterList bindableParameters;
	protected RequestParameterList bindableParameters = new RequestParameterList();
	
	public void process(HttpServletRequest request, HttpServletResponse response, SparkTask task) throws ParameterParseException, ParseException, IOException{
		SparkRequest r = new SparkRequest(request);
		r.parseParameters(bindableParameters);
		
		response.getWriter().print(task.process(r));
		response.getWriter().flush();
	}
}
