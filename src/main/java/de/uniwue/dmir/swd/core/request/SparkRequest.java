package de.uniwue.dmir.swd.core.request;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import de.uniwue.dmir.swd.core.request.exception.ParameterParseException;
import de.uniwue.dmir.swd.core.request.parameter.RequestParameter;

public class SparkRequest {

	private RequestParameterList parameters;
	private HttpServletRequest request;
	
	public SparkRequest(HttpServletRequest request){
		
		if( request == null ){
			throw new NullPointerException();
		}
		
		this.request = request;
		this.parameters = null;
	}
	
	public HttpServletRequest getRequest(){
		return request;
	}
	
	public RequestParameterList getParameters(){
		return parameters;
	}
	
	public boolean hasParameter(String name){
		for ( RequestParameter parameter: parameters ){
			if( parameter.getName() == name){
				return true;
			}
		}
		
		return false;
	}
	
	public RequestParameter getParameter(String name){
		for ( RequestParameter parameter: parameters ){
			if( parameter.getName() == name){
				return parameter;
			}
		}
		
		return null;
	}
	
	public void parseParameters(RequestParameterList bindableParameters) throws ParameterParseException, ParseException{
		this.parameters = RequestParameterFactory.create(request, bindableParameters);
	}
}
